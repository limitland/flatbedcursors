Version 0.6.0
=============

:Released: 2024-03-24
:Maintainer: Jens Luetkens <j.luetkens@limitland.de>

* Simplified resize cursors
* Expanded css tests


Version 0.5.2
=============

:Released: 2020-07-16
:Maintainer: Jens Luetkens <j.luetkens@limitland.de>

* Refinement for dark backgrounds


Version 0.5.1
=============

:Released: 2020-06-12
:Maintainer: Jens Luetkens <j.luetkens@limitland.de>

* Solve problems with the automated build pipeline, causing currupted images in the build artifacts.


Version 0.5
=============

:Released: 2018-08-06
:Maintainer: Jens Luetkens <j.luetkens@limitland.de>

* Minor updates to the build scripts.
* Remove XFree86 requirement for distribution PRMs.
* Add gitlab.com CI/CD pipeline and page configuration.


Version 0.4
============

:Released: 2016-06-06
:Maintainer: Jens Luetkens <j.luetkens@hamburg.de>

* Maintainability improvements of build system and programs.

* Render images using ‘rsvg-convert’ (faster than Inkscape).

* Use multisize cursors.

* Generate animation frames from starting image using SVG transform
  attributes (not ‘patch’).


Version 0.3
===========

:Released: 2010-06-16
:Maintainer: Jens Luetkens <j.luetkens@hamburg.de>

* license changed back to GPLv3


Version 0.2
===========

:Released: 2009-06-19
:Maintainer: Jens Luetkens <j.luetkens@hamburg.de>

* license changed to LGPLv3

* fix some firefox cursor hashes (thanx to tks)


Version 0.1
===========

:Released: 2005-01-17
:Maintainer: Jens Luetkens <j.luetkens@hamburg.de>

* initial release, 4 sizes, 5 colors


..
    Local variables:
    coding: utf-8
    mode: text
    mode: rst
    End:
    vim: fileencoding=utf-8 filetype=rst :
