# The original Flatbed Cursors sources README

The official FlatbedCursors releases are available on gitlab.com:
<https://gitlab.com/limitland/flatbedcursors>

The latest build artifacts (releases) are available on gitlab pages:
<https://limitland.gitlab.io/flatbedcursors>


INSTALLATION
------------

See the file INSTALL for installation instructions.


CUSTOMIZE YOUR CURSORS
----------------------

Despite the actual svg files a lot of customization can be
done when building the cursors.

All configuration options are in the "CONFIG" file, which gets
processed by the install scripts.

Edit the CONFIG files for colors, transparency and various other options.


COMPLETE INSTALLATION
---------------------

Use the installation script:

    $ ./install-all


BUILDING AN RPM
---------------

Use the distribution script to build RPMs:

    $ sudo ./build-distribution

Due to the installation path of /usr/share and no binary dependency the RPM
can get build as "noarch" version.


NAMING CURSORS
--------------

See the FlatbedCursors sources cursorlinks file.
