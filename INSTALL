Flatbed Cursors installation instructions
#######################################

Basic Installation
==================

Requirements:
- ImageMagick <http://www.imagemagick.org/>
- librsvg <http://librsvg.sourceforge.net/>
- rsvg-convert
- python docutils (optional)

Download the FlatbedCursors sources from:

<https://gitlab.com/limitland/flatbedcursors>

Use "git clone" for the latest development release or download a tar archive.
Change into the download directory and unpack the sources::

    $ tar -xjvf FlatbedCursors-<version>.tar.bz2
    $ cd FlatbedCursors-<version>

where “<version>” is the version you downloaded.

When you have the required programs, issue::

    $ export MULTISIZE=true
    $ ./bin/build-cursors

This will read the basic cursors from the SVG files, add a blurred and
scaled shadow, tint the icons and compose them into a single PNG
image.

After all the images have been processed, you should invoke::

    $ make
    $ make install

to create the xcursor files and install them into you personal cursors
folder (‘$HOME/.icons/’).

Not that some files (e.g. the shadow images) will be cached to reduce
building overhead. So if you make changes in sources (e.g. edit the svg
files) you might have to clean the working tree::

    $ make clean

Making changes in the sources is not recommended, but if you need to,
make sure to match colors and transformations as processed by the
'build-cursors' script. Read 'Customizing Cursors' below if you want to
build a custom Cursor theme.

If you want to remove the custom cursor theme from its installed location,
invoke::

    $ make uninstall


Customizing Cursors
===================

Apart from the actual SVG files, a lot of customization can be done
while composing the images.

All configuration options are in the ‘.CONFIG’ file for each theme.
Each theme has one, and there is a ‘custom.CONFIG’ which is a template
for you to copy and customise to build a custom theme.

To start making a custom theme, choose a name and invoke::

    $ export THEMENAME="Purple-and-Green"
    $ make custom-theme

The environment variable ‘THEMENAME’ will be used by each of the
programs to know which theme you have specified. (This is why the
‘export’ command is necessary.) The above step will make the files
‘Purple-and-Green.CONFIG’ and ‘Purple-and-Green.theme’ in the
‘FlatbedCursorsConfigs/’ directory, if they did not already exist. Edit
each of these files to customise your new theme.

The ‘$THEMENAME.CONFIG’ file configures the theme-specific behaviour of the
‘bin/render-cursor-image’ program. Run the program with no parameters for a
list of options that can be specified.

With version 0.8 teh cursors can be rendered as multisize xcursors. That's
why the build process requires either the "MULTISIZE" environment variable
or the "CURSORSIZE" environment variable::

For single-size cursors use::

    $ unset MULTISIZE
    $ export CURSORSIZE=40

For the multisize version use::

    $ export MULTISIZE=true

The ‘bin/render-cursor-image’ program is used by the ‘bin/build-cursors’
program, so when you want to batch-process the SVG source images you can
either add as many options to the command line as you want, or edit the
default values in the ‘$THEMENAME.CONFIG’ file.

Edit the custom files ‘FlatbedCursorsConfigs/$THEMENAME.CONFIG’ and
‘FlatbedCursorsConfigs/$THEMENAME.theme’ files to your liking. Then build and
install your custom cursor theme invoking::

    $ ./bin/build-cursors
    $ make
    $ make install

If you want to remove your custom cursor theme from its installed location,
invoke::

    $ make uninstall


Complete Installation
=====================

To install all the original FlatbedCursors issue::

    $ ./install-all

which will process all the configurations in the
FlatbedCursorsConfigs directory and install all cursor
sets provided into your ~/.icons directory.

If you want to remove all the original FlatbedCursors from its installed
location, invoke::

    $ ./install-all -u


System Installation
===================

For system-wide installation you need to copy the cursor-
directories to you X11 lib directory. Issue these commands::

    $ whereis X11
    $ man 3 xcursor
    $ cd /usr/share/icons
    $ sudo cp -r ~/.icons/FlatbedCursors* .

Now this will interfere with the cursor theme in your ~/.icons
directory, which will have precedence. You will want to rename
the cursors directory and edit the index.theme file inside.

If you want to make this theme the system-wide default, edit
the file /etc/sysconfig/windowmanager to read::

    X_MOUSE_CURSOR="FlatbedCursors-White-Regular-Slim"

or whatever your (custom) cursors directory is named.


You can also download pre-built RPMs from the opendesktop.org site
above and install them with::

    $ sudo rpm -Uv FlatbedCursors-<version>.noarch.rpm


..
    Local variables:
    coding: utf-8
    mode: text
    mode: rst
    End:
    vim: fileencoding=utf-8 filetype=rst :
